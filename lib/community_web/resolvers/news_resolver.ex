defmodule CommunityWeb.NewsResolver do
  alias Community.News

  def all_links(_root, _args, _info) do
    links = News.list_links()
    {:ok, links}
  end

  def create_link(_root, args, _info) do
    # TODO: add detailed error message handling later
    case News.create_link(args) do
      {:ok, link} ->
        {:ok, link}

      _error ->
        {:error, "could not create link"}
    end
  end

  def find_link(_root, %{id: id}, _info) do
    case News.get_link!(id) do
      nil ->
        {:error, "Link not found"}

      link ->
        {:ok, link}
    end
  end

  def update_link(%{id: id, link: link_params}, _info) do
    News.get_link!(id)
    |> News.update_link(link_params)
  end

  def delete_link(%{id: id}, _info) do
    News.get_link!(id)
    |> News.delete_link()
  end
end
